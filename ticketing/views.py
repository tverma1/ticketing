from ticketing.models import Ticket
from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    return render(request, 'index.html')

def index2(request):
    return render(request, 'home.html')

def submit(request):
    if request.method == "POST":
        user_name = request.POST.get('username')
        body = request.POST.get('body')
        new_ticket =  Ticket(submitter = user_name, body = body)
        new_ticket.save()
        return HttpResponse('successfully submit ticket!')
    return render(request, 'submit.html')

def tickets(request):
    all_ticket =  Ticket.objects.all()
    return render(request, 'tickets.html', {'tickets': all_ticket})

def ticket(request, ticket_id):
    selected_ticket =  Ticket.objects.get(pk = ticket_id)
    return render(request, 'ticket.html', {'ticket': selected_ticket})